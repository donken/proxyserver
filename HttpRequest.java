/**
 * HttpRequest - HTTP request container and parser
 *
 * $Id: HttpRequest.java,v 1.2 2003/11/26 18:11:53 kangasha Exp $
 *
 */

import java.io.*;
import java.net.*;

public class HttpRequest {
    /** Help variables */
    final static String CRLF = "\r\n";
    final static int HTTP_PORT = 80;
    /** Store the request parameters */
    String method;
    String URI;
    String path;
    URI parsedURI;
    String version;
    String headers = "";
    /** Server and port */
    private String host;
    private int port;

    /** Create HttpRequest by reading it from the client socket 
     * @throws HttpResponseException 
     * @throws URISyntaxException */
    public HttpRequest(BufferedReader from) throws HttpResponseException, URISyntaxException {
	String firstLine = "";
	try {
	    firstLine = from.readLine();
	} catch (IOException e) {
	    System.out.println("Error reading request line: " + e);
	}
	String[] tmp;
	
	try {
	tmp = firstLine.split(" ");
	} catch (NullPointerException e) {
		throw new HttpResponseException("Bad input from client.");
	}
	
	if (tmp.length != 3)
	{
		throw new HttpResponseException("Bad input from client.");
	}
	
	method = tmp[0];
	URI = tmp[1];
	version = tmp[2];
	path = "/";
	
	port = HTTP_PORT;

	if (!method.equals("GET")) {
	    System.out.println("Error: Method not GET");
	}
	try {
	    String line = from.readLine();
	    while (line.length() != 0) {
		headers += line + CRLF;
		/* We need to find host header to know which server to
		 * contact in case the request URI is not complete. */
		if (line.startsWith("Host:")) {
		    tmp = line.split(" ");
		    if (tmp[1].indexOf(':') > 0) {
			String[] tmp2 = tmp[1].split(":");
			host = tmp2[0];
			port = Integer.parseInt(tmp2[1]);
		    } else {
			host = tmp[1];
			port = HTTP_PORT;
		    }
		}
		
		line = from.readLine();
	    }
	} catch (IOException e) {
	    System.out.println("Error reading from socket: " + e);
	    return;
	}
	
	// Parse the URI we were given using the Java URI class
	parsedURI = new URI(URI);
	
	// If we were given a host, then we don't need parsedURI to
	// get it. Otherwise, use parsed URI to get the host.
	if (host == null)
	{
		host = parsedURI.getHost();
	} else {
		URI = "http://" + host + parsedURI.getPath();
		path = parsedURI.getPath();
	}
	
	//System.out.println("Host to contact is: " + host + " at port " + port);
	//System.out.println("URI is: " + URI);
    }

    /** Return host for which this request is intended */
    public String getHost() {
	return host;
    }

    /** Return port for server */
    public int getPort() {
	return port;
    }

    /**
     * Convert request into a string for easy re-sending.
     */
    public String toString() {
	String req = "";
	req = method + " " + path + " " + version + CRLF;
	//req += "Host: " + host + CRLF;
	req += headers;
	/* This proxy does not support persistent connections */
	req += "Connection: close" + CRLF;
	req += CRLF;
	
	return req;
    }
}