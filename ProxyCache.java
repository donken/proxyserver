/**
 * ProxyCache.java - Simple caching proxy
 *
 * $Id: ProxyCache.java,v 1.3 2004/02/16 15:22:00 kangasha Exp $
 *
 */

import java.net.*;
import java.io.*;
import java.util.HashMap;


/**
 * ProxyCache is the primary driver for the program and contains the logic for the
 * rudimentary caching functionality.
 * 
 * @author David Onken
 *
 */
public class ProxyCache {
	/** Port for the proxy */
	private static int port;
	/** Socket for client connections */
	private static ServerSocket socket;
	private static HashMap<String, String> cache;

	/** Create the ProxyCache object and the socket */
	public static void init(int p) {
		port = p;
		cache = new HashMap<String, String>();
		try {
			socket = new ServerSocket(port);
		} catch (IOException e) {
			System.out.println("Error creating socket: " + e);
			System.exit(-1);
		}
	}
	
	public static FileClass getWebpage(String URL)
	{
		// If the URL is stored in the hashmap, then we have a copy.
		String cacheCheck = cache.get(URL);
		FileClass fileIn = null;
		
		if (cacheCheck != null)
		{
			System.out.println("Cache hit: " + Thread.currentThread().getId());
			
			// Parse the URL as a character string and use that as the save name
			String cacheStr = URL.replaceAll("[-+.^:,/]","");
			
			ObjectInputStream in = null;
			try {
				// Read the FileClass object in from the file.
				in = new ObjectInputStream(new FileInputStream("cache/" + cacheStr + ".cache"));
			    fileIn = (FileClass) in.readObject();
			    in.close();
			} catch (FileNotFoundException e) {
				System.out.println("Writer failed to function.");
				return null;
			} catch (IOException e) {
				System.out.println("Writer failed to function.");
				return null;
			} catch (ClassNotFoundException e) {
				System.out.println("Writer failed to function.");
				try {
					in.close();
				} catch (IOException e1) {
					System.out.println("Writer failed to function.");
					return null;
				}
				return null;
			}
		}
		
		return fileIn;
	} 
	
	public static boolean putWebpage(String URL, String content, byte[] content2)
	{
		
		System.out.println("Cache miss: " + Thread.currentThread().getId());
		String cacheStr = URL.replaceAll("[-+.^:,/]","");
		ObjectOutputStream out;
		FileClass classObject = new FileClass(content, content2);
		try {
			// Write the FileClass object to the file and flush the stream
			FileOutputStream tmp = new FileOutputStream("cache/" + cacheStr + ".cache");
			out = new ObjectOutputStream(tmp);
			out.writeObject(classObject);
		    out.flush();
		    out.close();
		} catch (FileNotFoundException e) {
			System.out.println("Writer failed to function.");
			return false;
		} catch (IOException e) {
			System.out.println("Writer failed to function. ");
			return false;
		}
		
		// Add the URL and location to the hashmap
		cache.put(URL, "cache/" + cacheStr + ".cache");
		
		return true;
	}

	/** Read command line arguments and start proxy */
	public static void main(String args[]) {
		int myPort = 6500;

		try {
			myPort = Integer.parseInt(args[0]);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Need port number as argument");
			System.exit(-1);
		} catch (NumberFormatException e) {
			System.out.println("Please give port number as integer.");
			System.exit(-1);
		}

		init(myPort);

		/**
		 * Main loop. Listen for incoming connections and spawn a new thread for
		 * handling them
		 */
		Socket client = null;
		System.out.println("Main started on " + Thread.currentThread().getId());

		while (true) {
			try {
				client = socket.accept();
				Thread t = new Thread(new ClientThread(client));
				t.start();

			} catch (IOException e) {
				System.out.println("Error reading request from client: " + e);
				/*
				 * Definitely cannot continue processing this request, so skip
				 * to next iteration of while loop.
				 */
				continue;
			}
		}

	}
}