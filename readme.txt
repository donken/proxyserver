Author: David Onken.

This project is based off of the Java example code 
included with the assignment. It was programmed 
atop the Java 1.7 compiler.

The terminal returns a great number of messages 
that are supposed to display the status of running 
threads. It can get confusing because there are so 
many threads running at once. I have noticed that 
threads hang around for a while on later versions of 
Firefox, most likely due to the active connection issue. 
Firefox 15 does not have this problem, that I know of.

Once compiled, the server can be run from command 
line. It takes in a single argument that it parses as a 
number for the port it will use.

Ending the execution will destroy the cache database 
(as per assignment spec), as such it will not retain that 
information between restarts.