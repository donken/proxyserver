
public class HttpResponseException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HttpResponseException() {
		
	}

	public HttpResponseException(String arg0) {
		super(arg0);
	}

}
