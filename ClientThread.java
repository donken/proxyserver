import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;

/**
 * ClientThread is an abstraction of the idea of a client. It
 * runs as a separate thread and has access to its own
 * "handle" function.
 * 
 * @author David Onken
 *
 */
public class ClientThread implements Runnable {

	private Socket _client;
	
	   public ClientThread(Socket client) {
		   _client = client;
	}

	   public void run() {
		   handle(_client);
	   }
	   
		public void handle(Socket client) {
			Socket server = null;
			HttpRequest request = null;
			HttpResponse response = null;
			
			// I had bugs with lingering sockets, so this supposedly has them
			// set to timeout at 20 seconds. I don't think it quite works.
			try {
				client.setSoTimeout(20000);
			} catch (SocketException e2) {
				e2.printStackTrace();
				return;
			}

			/*
			 * Process request. If there are any exceptions, then simply return and
			 * end this request. This unfortunately means the client will hang for a
			 * while, until it timeouts.
			 */
			System.out.println("Client connection established on thread: " + Thread.currentThread().getId());

			/* Read request */
			try {
				System.out.println("Reading Request: " + Thread.currentThread().getId());
				BufferedReader fromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
				request = new HttpRequest(fromClient);
				
			} catch (IOException e) {
				System.out.println("Error reading request from client: " + e);
				return;
			} catch (HttpResponseException e) {
				System.out.println("HttpRequest encountered an error: " + e.getMessage());
				
				try {
					client.close();
					System.out.println("Client close.");
				} catch (IOException e1) {
					System.out.println("Error closing client: " + e1);
				}
				
				return;
			} catch (URISyntaxException e) {
				System.out.println("HttpRequest encountered an error: " + e.getMessage());
				
				try {
					client.close();
					System.out.println("Client close.");
				} catch (IOException e1) {
					System.out.println("Error closing client: " + e1);
				}
				
				return;
			}
			
			// This checks for a cached copy of the webpage
			FileClass cacheCopy = ProxyCache.getWebpage(request.URI);
			String headerData = "";
			byte[] bodyData = null;
			
			// If it existed, then the headerData and bodyData for later are saved
			if (cacheCopy != null)
			{
				headerData = cacheCopy.getHeader();
				bodyData = cacheCopy.getBody();
			}
			
			
			/* Send request to server */
			try {
				System.out.println("Sending Request: " + Thread.currentThread().getId());
				if (cacheCopy == null)
				{
					/* Open socket and write request to socket */
					server = new Socket(request.getHost(), request.getPort());
					DataOutputStream toServer = new DataOutputStream(server.getOutputStream());
					toServer.writeBytes(request.toString());
					System.out.println(request.URI + " on " + Thread.currentThread().getId());
				}
				
			} catch (UnknownHostException e) {
				System.out.println("Unknown host: " + request.getHost());
				System.out.println(e);
				try {
					client.close();
					server.close();
				} catch (IOException e1) {
					System.out.println("Error closing client and server: " + e1);
				}
				return;
			} catch (IOException e) {
				System.out.println("Error writing request to server: " + e);
				return;
			}
			/* Read response and forward it to client */
			try {

				System.out.println("Forwarding: " + Thread.currentThread().getId());
				
				// If we received a cache copy, then ignore all the usual logic of reading the
				// Server's response. Otherwise, read from the server and store the data.
				if (cacheCopy == null)
				{
					DataInputStream fromServer = new DataInputStream(server.getInputStream());
					response = new HttpResponse(fromServer);
					bodyData = response.getBytes();
					headerData = response.toString();
					ProxyCache.putWebpage(request.URI, headerData, bodyData);
				}
				
				// Write the headers to the output stream
				DataOutputStream toClient = new DataOutputStream(client.getOutputStream());
				toClient.writeBytes(headerData);
				
				// This just ignores ending an empty body to the client
				if (bodyData.length > 1 && bodyData[0] != 0)
				{
					toClient.write(bodyData);
				}
				
				// Close the client and server (if the server was ever invoked)
				client.close();
				if (server != null)
				{
					server.close();
				}
				System.out.println("Connections closed: " + Thread.currentThread().getId());
				
			} catch (IOException e) {
				System.out.println("Error writing response to client: " + e);
				return;
			}
			System.out.println("Thread Ending: " + Thread.currentThread().getId());
		}
	}
