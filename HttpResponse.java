/**
 * HttpResponse - Handle HTTP replies
 *
 * $Id: HttpResponse.java,v 1.2 2003/11/26 18:12:42 kangasha Exp $
 *
 */

import java.io.*;

public class HttpResponse {
	final static String CRLF = "\r\n";
	/**
	 * Maximum size of objects that this proxy can handle. For the moment set to
	 * 100 KB. You can adjust this as needed.
	 */
	final static int MAX_OBJECT_SIZE = 200000;
	/** Reply status and headers */
	String version;
	int status;
	String statusLine = "";
	String headers = "";
	/* Body of reply */
	byte[] body = new byte[MAX_OBJECT_SIZE];
	byte[] tmp;

	/** Read response from server. */
	@SuppressWarnings("deprecation")
	public HttpResponse(DataInputStream fromServer) {
		/* Length of the object */
		int length = -1;
		boolean gotStatusLine = false;

		/* First read status line and response headers */
		try {
			String line = fromServer.readLine();
			while (line.length() != 0) {
				if (!gotStatusLine) {
					statusLine = line;
					gotStatusLine = true;
				} else {
					headers += line + CRLF;
				}

				/*
				 * Get length of content as indicated by Content-Length header.
				 * Unfortunately this is not present in every response. Some
				 * servers return the header "Content-Length", others return
				 * "Content-length". You need to check for both here.
				 */
				if (line.startsWith("Content-Length")
						|| line.startsWith("Content-length")) {
					String[] tmp = line.split(" ");
					length = Integer.parseInt(tmp[1]);
				}
				line = fromServer.readLine();
			}
		} catch (IOException e) {
			System.out.println("Error reading headers from server: " + e);
			return;
		}

		int bytesRead = 0;
		boolean EOF = true;
		int acc = 0;

		while (bytesRead < length || EOF) {
			try {
				body[acc++] = fromServer.readByte();
			} catch (EOFException e) {
				EOF = false;
			} catch (IOException e) {
				System.out.println("Error reading response body: " + e);
				return;
			}
			bytesRead++;

			// If we ever breach the maximum size, then just say it was too long
			if (acc >= MAX_OBJECT_SIZE) {
				System.out.println("Return body was too large.");
				return;
			}
		}
		
		// Put the large body buffer into a smaller array.
		tmp = new byte[bytesRead];
		for (acc = 0; acc < bytesRead; acc++)
		{
			tmp[acc] = body[acc];
		}

	}

	/**
	 * Convert response into a string for easy re-sending. Only converts the
	 * response headers, body is not converted to a string.
	 */
	public String toString() {
		String res = "";

		res = statusLine + CRLF;
		res += headers + CRLF;

		return res;
	}
	
	public byte[] getBytes() {
		return tmp;
	}
}